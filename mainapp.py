import os
import urllib

from google.appengine.api import users
from google.appengine.ext import ndb

import cgi
import webapp2
import jinja2

import modules.project_manifest 

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), 'views')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

DEFAULT_PROJECTMANIFEST_NAME = 'default_projectmanifest'

class MainPage(webapp2.RequestHandler):

    def get(self):

        blurb = "Login to continue"
        
        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
            user_logged = True
            self.redirect("/dashboard")
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'
            user_logged = False

        template_values = {
        	'user': user_logged,
            'url': url,
            'url_linktext': url_linktext,
            'content': blurb,
        }

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))

class DashboardPage(webapp2.RequestHandler):

    def get(self):

        projectmanifest_name = self.request.get('projectmanifest_name', DEFAULT_PROJECTMANIFEST_NAME)
        project_query = modules.project_manifest.Project.query(
        ancestor=modules.project_manifest.projectmanifest_key(projectmanifest_name)).order(-modules.project_manifest.Project.end_date)
        projects = project_query.fetch(10)

        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
            user_logged = True
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'
            user_logged = False
            self.redirect("/")

        template_values = {
            'user': user_logged,
            'projects': projects,
            'projectmanifest_name': urllib.quote_plus(projectmanifest_name),
            'url': url,
            'url_linktext': url_linktext,
        }

        print "dashboardpage"
        print modules.project_manifest.projectmanifest_key

        template = JINJA_ENVIRONMENT.get_template('dashboard.html')
        self.response.write(template.render(template_values))

application = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/dashboard', DashboardPage),
    ('/new', modules.project_manifest.NewProjectManifestEntry)
], debug=True)