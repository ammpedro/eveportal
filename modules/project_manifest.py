import os
import urllib

from google.appengine.api import users
from google.appengine.ext import ndb

import cgi
import webapp2
import jinja2

from datetime import datetime

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__), '../views')),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

DEFAULT_PROJECTMANIFEST_NAME = 'default_projectmanifest'

def projectmanifest_key(projectmanifest_name=DEFAULT_PROJECTMANIFEST_NAME):
    """
    Constructs a Datastore key for a ProjectManifest entity with projectmanifest_name.
    """
    return ndb.Key('NewProjectManifestEntry', projectmanifest_name)


class Project(ndb.Model):
    """
    Models a Project entry with author, title, location, special remarks, start date and end date.
    """
    author = ndb.UserProperty()
    name = ndb.StringProperty()
    location = ndb.StringProperty()
    remarks = ndb.StringProperty(indexed=False)
    create_date = ndb.DateTimeProperty(auto_now_add=True)
    start_date = ndb.DateProperty()
    end_date = ndb.DateProperty()


class NewProjectManifestEntry(webapp2.RequestHandler):

    def get(self):
        if users.get_current_user():
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
            user_logged = True
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'
            user_logged = False
            self.redirect("/")

        template_values = {
            'user': user_logged,
            'url': url,
            'url_linktext': url_linktext,
        }
        
        template = JINJA_ENVIRONMENT.get_template('manifest.html')
        self.response.write(template.render(template_values))
        

    def post(self):
        # We set the same parent key on the 'Project' to ensure each Project
        # is in the same entity group. Queries across the single entity group
        # will be consistent. However, the write rate to a single entity group
        # should be limited to ~1/second.
        projectmanifest_name = self.request.get('projectmanifest_name', DEFAULT_PROJECTMANIFEST_NAME)
        project = Project(parent=projectmanifest_key(projectmanifest_name))

        if users.get_current_user():
            project.author = users.get_current_user()
            project.name = self.request.get('name')
            project.location = self.request.get('location')
            project.start_date = datetime.strptime(self.request.get('start_date'),'%Y-%m-%d')
            project.end_date = datetime.strptime(self.request.get('end_date'),'%Y-%m-%d')
            project.remarks = self.request.get('remarks')
            project.put()
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
            user_logged = True
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'
            user_logged = False
            self.redirect("/")

        template_values = {
            'user': user_logged,
            'url': url,
            'url_linktext': url_linktext,
        }

        print "project_manifest"
        print projectmanifest_key

        query_params = {'projectmanifest_name': projectmanifest_name}
        self.redirect('/dashboard?' + urllib.urlencode(query_params))
